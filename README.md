# MBZIRC Truck State Prediction #

### What is this repository for? ###

This code provides the state prediction of the truck (deck) for MBZIRC Challenge. The code only provides the prediction according to the path defined for the moving truck.

### How do I get set up? ###

To use the code in the simulation environment, please consult the [Simulation Readme](https://bitbucket.org/castacks/mbzirc_simulation).

### Who do I talk to? ###

* Guilherme Pereira (gpereira@ufmg.br)
* Azarakhsh Keipour (keipour@gmail.com)