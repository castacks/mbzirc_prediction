#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <fstream>

namespace ca {

class Track{   // This class defines the track for MBZIRC competition as a series of waypoints
 private:
 
  double radius; // Radius of 8
  
 public:
     std::vector<nav_msgs::Odometry> waypoints;
     double lndelta; // Straight line distance between two waypoints
     
     Track(double xorg, double yorg, double theta){ // Constructor
       	radius=20;
       	lndelta=0.2094; // Distance between waypoints
       	nav_msgs::Odometry wp;
        
       	// This aproximates the track by a series of waypoints

  	// Generates the first straight line -> starts at (0,0)
  	for (double i=0; i<radius; i=i+lndelta){ 
            
            // Original point before rotation and translation  
	    double x=i*cos(M_PI/4);
            double y=i*sin(M_PI/4);
            
            // Rotation and translation     
	    wp.pose.pose.position.x=x*cos(theta)-y*sin(theta)+xorg;
 	    wp.pose.pose.position.y=x*sin(theta)+y*cos(theta)+yorg;		
	    wp.pose.pose.position.z=0.0;
            wp.header.frame_id = "/world";
    	    waypoints.push_back(wp);
  	}

  	// First curve
  	for (double ang=M_PI/2+M_PI/4; ang>=-(M_PI/2+M_PI/4); ang-=M_PI/300){

	   // Original point before rotation and translation  	
	   double x=radius*cos(ang)+(radius/cos(M_PI/4));
	   double y=radius*sin(ang);	
            
           // Rotation and translation     
	   wp.pose.pose.position.x=x*cos(theta)-y*sin(theta)+xorg;
 	   wp.pose.pose.position.y=x*sin(theta)+y*cos(theta)+yorg;
    	   wp.pose.pose.position.z=0.0;
	   wp.header.frame_id = "/world";
    	   waypoints.push_back(wp);
  	}

  	// Second straight line
  	for (double i=-(radius-lndelta); i<radius; i=i+lndelta){

           // Original point before rotation and translation  	
	   double x=-i*cos(M_PI/4);
	   double y=i*sin(M_PI/4);

           // Rotation and translation     
	   wp.pose.pose.position.x=x*cos(theta)-y*sin(theta)+xorg;
 	   wp.pose.pose.position.y=x*sin(theta)+y*cos(theta)+yorg;   
    	   wp.pose.pose.position.z=0.0;
           wp.header.frame_id = "/world";
    	   waypoints.push_back(wp);
  	}

  	// Second curve
        for (double ang=M_PI/2-M_PI/4; ang<=(M_PI+M_PI/2+M_PI/4); ang+=M_PI/300){

           // Original point before rotation and translation  	
	   double x=radius*cos(ang)-(radius/cos(M_PI/4));
	   double y=radius*sin(ang);

           // Rotation and translation     
	   wp.pose.pose.position.x=x*cos(theta)-y*sin(theta)+xorg;
 	   wp.pose.pose.position.y=x*sin(theta)+y*cos(theta)+yorg; 
    	   wp.pose.pose.position.z=0.0;
           wp.header.frame_id = "/world"; 
    	   waypoints.push_back(wp);
  	}

  	// Third straight line -> Finishes at (0,0)
  	for (double i=-(radius-lndelta); i<0; i=i+lndelta){

	   // Original point before rotation and translation  	
	   double x=i*cos(M_PI/4);
	   double y=i*sin(M_PI/4);

           // Rotation and translation     
	   wp.pose.pose.position.x=x*cos(theta)-y*sin(theta)+xorg;
 	   wp.pose.pose.position.y=x*sin(theta)+y*cos(theta)+yorg;    
    	   wp.pose.pose.position.z=0.0;
           wp.header.frame_id = "/world";
    	   waypoints.push_back(wp);
  	}
    	//std::ofstream myfile;
  	//myfile.open ("/home/gpereira/track.txt");
	//for (int i=0; i<waypoints.size(); i++)
  	//	myfile << waypoints[i].pose.pose.position.x << " " << waypoints[i].pose.pose.position.y << "\n";
  	//myfile.close();
       	
     } 

     // Distance in 2D
     double Distance2(const nav_msgs::Odometry a, const nav_msgs::Odometry b) {
   		return sqrt(pow(a.pose.pose.position.x-b.pose.pose.position.x, 2) + pow(a.pose.pose.position.y-b.pose.pose.position.y, 2) );
     }
     
     int Project(nav_msgs::Odometry pose){  // Project pose into the track and return the index of the closest node
	  int closest = 0;
  	  double min_dist = Distance2(pose, waypoints[closest]); 
  	  for (int i=1; i<waypoints.size(); i++)
		if (Distance2(pose, waypoints[i])<min_dist){
			closest = i;
			min_dist=Distance2(pose, waypoints[closest]);      
		}
         return closest;  
      }

     int Project(nav_msgs::Odometry pose, int around_index){  // Project pose into the track and return the index of the closest node
	  int closest = around_index-waypoints.size()/5;
          if (closest < 0)  closest+=waypoints.size();
  	  double min_dist = Distance2(pose, waypoints[closest]);
          int start=closest;
          int end = (around_index+waypoints.size()/5)%waypoints.size();
  	  ROS_INFO("Start %d End %d", start, end);  
          for (int i=start; i!=end; i=(i+1)%waypoints.size())
		if (Distance2(pose, waypoints[i])<min_dist){
			closest = i;
			min_dist=Distance2(pose, waypoints[closest]);      
		}
         return closest;  
      } 
     
    
      bool inTrack(nav_msgs::Odometry pose, double delta){ // Check if the pose is on the track
          for (int i=1; i<waypoints.size(); i++)
		if (Distance2(pose, waypoints[i])<delta){
			return true;  
		}
         return false;  
     
     }
};

}
