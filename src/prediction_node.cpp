#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <vector>
#include <math.h>
#include "tf/transform_datatypes.h"
#include <tf/transform_listener.h>
#include <iostream>
#include <fstream>
#include "mbzirc_prediction/GetPrediction.h"
#include "mbzirc_prediction/trackDefinition.h"
#include "math_utils/math_utils.h"
#include <ca_common/math.h>


// This is the callback for Deck Pose
nav_msgs::Odometry curr_deck_pose;
bool got_pose = false;
void GetDeckPose(const nav_msgs::Odometry::ConstPtr &msg) {
  curr_deck_pose = *msg;
  got_pose = true;
}


// Distance in 2D
double Distance2(const nav_msgs::Odometry a, const nav_msgs::Odometry b) {

   return sqrt(pow(a.pose.pose.position.x-b.pose.pose.position.x, 2) + pow(a.pose.pose.position.y-b.pose.pose.position.y, 2) );
}


class Predictor{
 private:
 
  ca::Track *track;
  double curr_speed;
  nav_msgs::Odometry curr_pose, prev_pose;
  std::vector<double> speed_buff;
  int nbuff; 
  int curr_pose_idx; 
 
 public:
     Predictor(double xorg, double yorg, double theta){ // Constructor
       	
        nbuff=3; // size of the speed buffer - used for median;
        track = new ca::Track(xorg, yorg, theta); 
     } 
  

   // Set the poses used in the prediction
   void setPose(const nav_msgs::Odometry p, const nav_msgs::Odometry pant)
    {
   	curr_pose=p;
        prev_pose=pant; 

	// Finds the closest waypoint to the previous deck position
        int prev_pose_idx=track->Project(prev_pose);   

	// Finds the closest waypoint to the current deck position
        int temp_curr_pose_idx=track->Project(curr_pose);

        int dist_idx = (temp_curr_pose_idx - prev_pose_idx);      // Distance between current and previous pose in number of waypoints

	// Fix wrap-around problems
        if (abs(dist_idx) > (track->waypoints.size()/2))
		if (dist_idx<0) 
		     dist_idx=track->waypoints.size()+dist_idx;
		else 
		     dist_idx=track->waypoints.size()-dist_idx;
        
        // Check for weird situations - most of them caused by the center of the 8
        if ((abs(dist_idx) > (track->waypoints.size()/10)) ||  // Too fast! 
            (curr_speed*dist_idx<0.0)                     ||   // Sudden change on the direction! 
            (dist_idx==0.0))                                   // Too slow!
        {  
        	return; //TODO - Maybe we should update the curr_pose_idx with a predicted position
	}
	else
           curr_pose_idx=temp_curr_pose_idx; // Only changes the curr_pose_idx if it makes sense. This is to avoid problems in the center of the 8

       // ROS_INFO("Prev %d Curr %d Dist %d ", prev_pose_idx, curr_pose_idx, dist_idx);


	ros::Duration delta_t = curr_pose.header.stamp - prev_pose.header.stamp; 
        double secs = delta_t.toSec();
        if (abs(dist_idx)>1)	// Current deck speed for higher speeds	             
            curr_speed = dist_idx*track->lndelta/secs;                  
        else{			// Current deck speed for lower speeds - current and previous pose are in the same waypoint
            CA::Vector3D deck_velocity=CA::msgc(curr_pose.pose.pose.position)-CA::msgc(prev_pose.pose.pose.position);
	    deck_velocity.normalize();
	    deck_velocity*=track->lndelta;
    	    CA::Vector3D next_position = CA::msgc(curr_pose.pose.pose.position)+deck_velocity;
            int next_idx=(curr_pose_idx+1)%track->waypoints.size();
            int prev_idx=curr_pose_idx-1;
	    if (prev_idx < 0)  prev_idx+=track->waypoints.size();
            if ((next_position-CA::msgc(track->waypoints[next_idx].pose.pose.position)).norm()<(next_position-CA::msgc(track->waypoints[prev_idx].pose.pose.position)).norm())
		curr_speed = Distance2(curr_pose, prev_pose)/secs;
	    else
 		curr_speed = -Distance2(curr_pose, prev_pose)/secs;		
	}

        curr_speed=filter(curr_speed);                      // A median filter is used to filter the speed

    }	

   // Median filter on the speed 
   double filter(double speed)
    {
 	  std::vector<double> local_speed_buff;        
 
          if (speed_buff.size()==nbuff){ // Full buffer 
          	speed_buff[0]=speed;

                local_speed_buff=speed_buff; // Copy the buffer to sort
	       	sort(local_speed_buff.begin(), local_speed_buff.end());      
          	speed=local_speed_buff[local_speed_buff.size()/2];

		for (int i=speed_buff.size()-1; i>0; i--){ // Shift the buffer
		    speed_buff[i]=speed_buff[i-1];	
		}
          }
	  else
	       speed_buff.push_back(speed); // Fill the buffer
     
          return speed;
    }	
   

   // This is the callback for the service - Prediction assumes that the deck is on the track!
   bool predict(mbzirc_prediction::GetPrediction::Request  &req,  mbzirc_prediction::GetPrediction::Response &res)
    {
        
        // Prediction     
	double length=req.time*curr_speed;                    // Lenght (negative or positive) the deck would move considering the prediction time                
        int d_idx = round(length/track->lndelta);             // Number of waypoints the deck would move considering the prediction time 
 	
	int next_pos_idx = curr_pose_idx+d_idx;
	if (next_pos_idx < 0) next_pos_idx=track->waypoints.size()+next_pos_idx;
        next_pos_idx = next_pos_idx%track->waypoints.size();
        
	int sign_d_idx=1;
	if (d_idx<0) sign_d_idx=-1; 			     // moving in the negative direction 
        int next_next_pos_idx =curr_pose_idx+d_idx+sign_d_idx;
	if (next_next_pos_idx < 0) next_next_pos_idx=track->waypoints.size()+next_next_pos_idx;
        next_next_pos_idx = next_next_pos_idx%track->waypoints.size();

        res.pose_ahead=track->waypoints[next_pos_idx];
        CA::Vector3D deck_velocity=CA::msgc(track->waypoints[next_next_pos_idx].pose.pose.position)-CA::msgc(track->waypoints[next_pos_idx].pose.pose.position);
        deck_velocity.normalize(); 
        res.pose_ahead.twist.twist.linear=CA::msgc(deck_velocity*fabs(curr_speed));
        CA::Vector3D attitude;
        attitude[0]=attitude[1]=0;
        attitude[2]=atan2(deck_velocity[1],deck_velocity[0]);// heading
        res.pose_ahead.pose.pose.orientation=CA::msgc(CA::eulerToQuat(attitude));

  	return true;
    }	

};

int main(int argc, char **argv) {

  ros::init(argc, argv, "prediction_node");

  ros::NodeHandle n("~");
  
  ros::Duration(1.0).sleep(); 

  // Get transform with track position and orientation
  double x_origin, y_origin, orientation;
  tf::TransformListener listener;
  tf::StampedTransform transform;

  if (listener.waitForTransform("/world", "/track", ros::Time(0), ros::Duration(5.0)) ) {
       ROS_INFO("Prediction - Using available transformation for track");
       listener.lookupTransform("/world", "/track", ros::Time(0), transform);
       x_origin = transform.getOrigin().x();
       y_origin = transform.getOrigin().y();	
       orientation = transform.getRotation().getAngle();
       ROS_INFO("X: %f Y: %f Theta: %f", x_origin, y_origin, orientation);
  }
  else{
       ROS_WARN("Prediction - No transformation received - Assuming track at (0,0)");
       x_origin = 0.0;
       y_origin = 0.0;	
       orientation = 0.0;
  }
  

  Predictor predictor(x_origin, y_origin, orientation);
  ros::ServiceServer service = n.advertiseService("prediction", &Predictor::predict, &predictor);

  ros::Subscriber target_pos_sub = n.subscribe<nav_msgs::Odometry>("/deck/odom", 1, GetDeckPose);

  ros::Rate loop_rate(15);


  // Wait for the initial pose
  while(!got_pose)
  {
      ros::spinOnce();
      loop_rate.sleep();
  }

  
  nav_msgs::Odometry prev_deck_pose = curr_deck_pose;

  while (ros::ok())
     {
      
      ros::spinOnce();

      if (curr_deck_pose.header.stamp != prev_deck_pose.header.stamp){
          	   
           predictor.setPose(curr_deck_pose, prev_deck_pose); 
           prev_deck_pose = curr_deck_pose;	

      }

      
      loop_rate.sleep();
    }
}

